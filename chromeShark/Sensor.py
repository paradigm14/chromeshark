from pgi.repository import Gdk


def read_pixel(x, y):
    window = Gdk.get_default_root_window()
    pixel_buffer = Gdk.pixbuf_get_from_window(window, x, y, 1, 1)
    return pixel_buffer.get_pixels()
