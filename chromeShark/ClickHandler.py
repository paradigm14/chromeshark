from pymouse import PyMouseEvent


class ClickHandler(PyMouseEvent):

    def __init__(self):
        PyMouseEvent.__init__(self)
        self.click_pos = None, None

    def click(self, x, y, button, press):
        if button == 1 and press:
            self.click_pos = x, y
            raise Exception


def wait_for_click():
    try:
        mouse = ClickHandler()
        mouse.run()
    except:
        return mouse.click_pos
