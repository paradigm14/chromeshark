from pykeyboard import PyKeyboard
keyboard = PyKeyboard()


class Dino:

    def __init__(self):
        self._keyboard = PyKeyboard()

    def jump(self):
        self._keyboard.tap_key(keyboard.up_key)
