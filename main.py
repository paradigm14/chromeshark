#!/usr/bin/env python3
from chromeShark.ClickHandler import wait_for_click
from chromeShark.Dino import Dino
from chromeShark.Sensor import read_pixel

import time


print("Click on dino's top left boundary")
x1, y1 = wait_for_click()

print("Click on dino's bottom right boundary")
x2, y2 = wait_for_click()

print("Click on any foreground pixel")
x, y = wait_for_click()
fg_pixel = read_pixel(x, y)
print(fg_pixel)

print("Click on any background pixel")
x, y = wait_for_click()
bg_pixel = read_pixel(x, y)
print(bg_pixel)

print("Click on sensor position")
xS, yS = wait_for_click()
print((xS, yS))

dino = Dino()
dino.jump()

while True:
    current_px = read_pixel(xS, yS)

    if (current_px[0] == fg_pixel[0]):
        dino.jump()
